#!/usr/bin/env node
const fs = require('fs');
const git = require('simple-git/promise');

doIt();
async function doIt() {
	fs.readdirSync(`${process.cwd()}/.git/`);
	const amendNeed = fs.readdirSync(`${process.cwd()}/.git/`);
	if(amendNeed.indexOf('needAmend') !== -1){
		const gitRepos = git(process.cwd());
		await gitRepos.add(['package.json']);
		fs.unlinkSync(`${process.cwd()}/.git/needAmend`);
		await gitRepos.raw('commit --amend -C HEAD --no-verify'.split(' '));
	}
}
